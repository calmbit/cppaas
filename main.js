var app = require('express')();
var multer = require('multer');
const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, 'uploads/')
	},
	filename: function(req, file, cb) {
		cb(null, file.fieldname + "-" + Date.now())
	}
});
var upload = multer({storage});
var fs = require('fs');
const { exec } = require('child_process');

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/home.html');
});

app.post('/send', upload.single('thefile'), function(req, res) {
	exec(`cat ${req.file.path} | cpp -P && rm ${req.file.path}`, function(err, stdout, stderr) {
		if (err) {
			throw err;
		}
		res.send(`${stdout}`);
		console.log(`${stderr}`);
	});
});

app.listen(8080, function() {
	console.log("Now listening on 8080...");
});
